using Microsoft.AspNetCore.Mvc;
using ServiceLayer.Interfaces;
using System.IO;

namespace ApiAwsStuff.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Entrycontroller : ControllerBase
    {
        private readonly ILogger<Entrycontroller> _logger;
        private readonly ISecretService _secretService;
        private readonly IBucketService _bucketService;

        public Entrycontroller(ILogger<Entrycontroller> logger,
                                ISecretService secretService,
                                IBucketService bucketService)
        {
            _logger = logger;
            _secretService = secretService;
            _bucketService = bucketService;
        }

        [HttpGet]
        public async Task<string> GetSecret()
        {
            return await _secretService.GetSecret("s3_endpoint_secretz");
        }

        [HttpGet]
        [Route("logger")]
        public void GetLogger()
        {
            _logger.LogInformation("test3");
        }


        [HttpGet]
        [Route("bucket")]
        public async Task<FileStreamResult> GetBucket()
        {
            var response = await _bucketService.GetFile();

            var contentType = "image/png";

            return File(response, contentType, "image.png");
        }
    }
}