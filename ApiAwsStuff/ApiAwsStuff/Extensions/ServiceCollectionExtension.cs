﻿using Amazon.S3;
using Amazon.SecretsManager;
using ServiceLayer.Interfaces;
using ServiceLayer.Services;

namespace ApiAwsStuff.ServiceCollectionExtension
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<ISecretService, SecretService>();
            services.AddTransient<IBucketService, BucketService>();
            
            return services;
        }

        public static IServiceCollection AddAwsServices(this IServiceCollection services)
        {            
            services.AddAWSService<IAmazonS3>();
            services.AddAWSService<IAmazonSecretsManager>();

            return services;
        }
    }
}
