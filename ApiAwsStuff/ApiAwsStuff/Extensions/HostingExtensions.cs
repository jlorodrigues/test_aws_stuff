﻿using ApiAwsStuff.ServiceCollectionExtension;
using Microsoft.Extensions.Configuration;

namespace ApiAwsStuff.Extensions
{
    internal static class HostingExtensions
    {
        public static WebApplicationBuilder ConfigureServices(this WebApplicationBuilder builder)
        {
            builder.Services.AddAwsServices();

            builder.Services.AddServices();

            builder.Services.AddLogging(config =>
            {
                config.AddAWSProvider(builder.Configuration.GetAWSLoggingConfigSection());
                config.SetMinimumLevel(LogLevel.Debug);
            });

            return builder;
        }
    }
}
