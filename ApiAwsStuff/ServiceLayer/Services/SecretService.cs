﻿using Amazon.SecretsManager;
using ServiceLayer.Interfaces;

namespace ServiceLayer.Services
{
    public class SecretService : ISecretService
    {
        public IAmazonSecretsManager _secretsClient;

        public SecretService(IAmazonSecretsManager secretsClient)
        {
            _secretsClient = secretsClient;
        }

        public async Task<string> GetSecret(string secret)
        {
            var request = new Amazon.SecretsManager.Model.GetSecretValueRequest
            {
                SecretId = secret
            };

            var secretToReturn = await _secretsClient.GetSecretValueAsync(request);

            return secretToReturn.SecretString;
        }
    }
}
