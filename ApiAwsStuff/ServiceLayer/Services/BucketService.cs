﻿using Amazon.S3;
using Amazon.S3.Model;
using ServiceLayer.Interfaces;

namespace ServiceLayer.Services
{
    public class BucketService : IBucketService
    {
        public IAmazonS3 _s3Client;

        public BucketService(IAmazonS3 s3Client)
        {
            _s3Client = s3Client;
        }

        public async Task<Stream> GetFile()
        {
            var request = new GetObjectRequest
            {
                BucketName = "elasticbeanstalk-eu-west-2-049256117758",
                Key = "image.png"
            };

            var response = await _s3Client.GetObjectAsync(request);
            return response.ResponseStream;
        }
    }
}
