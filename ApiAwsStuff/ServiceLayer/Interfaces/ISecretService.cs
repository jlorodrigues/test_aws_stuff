﻿namespace ServiceLayer.Interfaces
{
    public interface ISecretService
    {
        public Task<string> GetSecret(string secret);
    }
}
