﻿namespace ServiceLayer.Interfaces
{
    public interface IBucketService
    {
        public Task<Stream> GetFile();
    }
}
